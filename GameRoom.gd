extends Node

signal playerMovementData
signal playerJoinLeaveData
signal currentPlayerAssignId

func _ready():
	WsClient.connect("message", self, "_on_message")

func _on_message(data):
	data = parse_json(data)
	if data['MESSAGE_TYPE'] == 'ASSIGN_ID':
		var id = data['id']
		emit_signal("currentPlayerAssignId", id, data['PLAYERS'])
	elif data['MESSAGE_TYPE'] == 'JOINED_ROOM':
		var id = data['id']
		emit_signal("playerJoinLeaveData", id, true)
	elif data['MESSAGE_TYPE'] == 'LEFT_ROOM':
		var id = data['id']
		emit_signal("playerJoinLeaveData", id, false)
	elif data['MESSAGE_TYPE'] == 'PLAYER_MOVE':
		var id = data['id']
		emit_signal("playerMovementData", id, data['MOTION']["x"], data['MOTION']["y"], data['POSITION']["x"], data['POSITION']["y"])
	
func update_player_position(id, x, y, px, py):
	var json = {
		"MESSAGE_TYPE" : "PLAYER_MOVE",
		"id" : id,
		"MOTION" : {
			"x" : x,
			"y" : y
		},
		"POSITION" : {
			"x" : px,
			"y" : py
		}
	}
	WsClient.send_data(json)
