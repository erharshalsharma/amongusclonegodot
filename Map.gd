extends Node2D

var Character = load("res://Character.tscn")
var characterDisplacement = Vector2(0,0)
const SPEED = 1000
onready var Camera = $Camera2D
var CurrentPlayer : KinematicBody2D
var CurrentPlayerID
var lastMotion = Vector2(0,0)
var playerMap = {}

# loades the packaged scene

func _ready():
	create_current_player()
	GameRoom.connect("playerJoinLeaveData", self, "_on_player_join_leave")
	GameRoom.connect("playerMovementData", self, "_on_player_move")
	GameRoom.connect("currentPlayerAssignId", self, "_on_assign_id")
	

func create_current_player():
	var instance = Character.instance()
	instance.position.x = 0
	instance.position.y = 0
	self.add_child(instance)
	$Camera2D.clear_current()
	instance.get_node("Camera2D").make_current()
	CurrentPlayer = instance

func create_player(id, x=0,y=0):
	var instance = Character.instance()
	instance.position.x = x
	instance.position.y = y
	self.add_child(instance)
	playerMap[id] = instance

func remove_player(id):
	if playerMap.has(id):
		playerMap[id].queue_free()
		playerMap.erase(id)

func _process(delta):
	move_current_player()
#	move_other_player()

func move_current_player():
	if Input.is_action_pressed("left") and not Input.is_action_pressed("right"):
		characterDisplacement.x = -SPEED
	elif Input.is_action_pressed("right") and not Input.is_action_pressed("left"):
		characterDisplacement.x = SPEED
	else:
		characterDisplacement.x = 0
	
	if Input.is_action_pressed("up") and not Input.is_action_pressed("down"):
		characterDisplacement.y = -SPEED
	elif Input.is_action_pressed("down") and not Input.is_action_pressed("up"):
		characterDisplacement.y = SPEED
	else:
		characterDisplacement.y = 0
		
	CurrentPlayer.motion = characterDisplacement
	send_player_pos_ifchanged(characterDisplacement)	

func send_player_pos_ifchanged(motion):
		if lastMotion.x != motion.x || lastMotion.y != motion.y:
			GameRoom.update_player_position(CurrentPlayerID, motion.x, motion.y, CurrentPlayer.position.x, CurrentPlayer.position.y)
			lastMotion = motion

func _on_player_move(id, x, y, px, py):
	if playerMap.has(id) == true:
		playerMap[id].motion.x = x
		playerMap[id].motion.y = y
		playerMap[id].position.x = px
		playerMap[id].position.y = py
		
func _on_player_join_leave(id, status):
	if status == true and not playerMap.has(id):
		create_player(id)
	else:
		remove_player(id)

func _on_assign_id(id, players):
	CurrentPlayerID = id
	for player in players:
		if not playerMap.has(player['id']) and player['id'] != CurrentPlayerID:
			create_player(player['id'], player['position']['x'], player['position']['y'])
		else:
			_on_player_move(player['id'], player['motion']['x'], player['motion']['y'], player['position']['x'], player['position']['y'])
			
