extends KinematicBody2D

export var motion = Vector2(0,0)

func _physics_process(delta):
	move_and_slide(motion)
